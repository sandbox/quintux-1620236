<?php
/**
 * @file
 * Commerce Draft Carts module allows user to create and manage draft shopping
 * carts.
 */

/**
 * Add the text field for the cart name in commerce orders.
 */
function commerce_draft_carts_configure_order_fields($type = 'commerce_order') {
  // Add the order draft cart field.
  commerce_draft_carts_name_create_instance('commerce_draft_cart_name', 'commerce_order', $type, t('Cart name'), -8, array('type' => 'text'));
}

/**
 * Attach the text field for the cart name in commerce orders.
 */
function commerce_draft_carts_name_create_instance($field_name, $entity_type, $bundle, $label, $weight = 0, $display = array()) {
  // If a field type we know should exist isn't found, clear the Field cache.
  if (!field_info_field_types('text')) {
    field_cache_clear();
  }

  // Look for or add the specified price field to the requested entity bundle.
  $field = field_info_field($field_name);
  $instance = field_info_instance($entity_type, $field_name, $bundle);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'text',
      'cardinality' => 1,
      'entity_types' => array($entity_type),
      'translatable' => FALSE,
      'locked' => TRUE,
    );
    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => $label,
      'required' => FALSE,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => 0,
      ),
      // Because this widget is locked, we need it to use the full price widget
      // since the currency option can't be adjusted at the moment.
      'widget' => array(
        'type' => 'text_textfield',
        'weight' => $weight,
        'settings' => array(
          'size' => '60',
        ),
      ),
      'display' => array(),
    );

    $entity_info = entity_get_info($entity_type);

    // Spoof the default view mode and node teaser so its display type is set.
    $entity_info['view modes'] += array(
      'default' => array(),
      'node_teaser' => array(),
    );

    foreach ($entity_info['view modes'] as $view_mode => $data) {
      $instance['display'][$view_mode] = $display + array(
        'label' => 'hidden',
        'type' => 'hidden',
        'settings' => array(),
        'weight' => $weight,
      );
    }
    $instance['display']['default']['type'] = 'hidden';

    field_create_instance($instance);
  }
}

/**
 * Implements hook_commerce_order_status_info().
 *
 * Add draft cart status
 */
function commerce_draft_carts_commerce_order_status_info() {
  $order_statuses = array();

  $order_statuses['cart_draft'] = array(
    'name' => 'cart_draft',
    'title' => t('Shopping cart: Draft'),
    'state' => 'cart',
    'cart' => FALSE,
    'weight' => -1,
    'status' => TRUE,
  );

  return $order_statuses;
}

/**
 * Implements hook_permission().
 */
function commerce_draft_carts_permission() {
  $permissions = array(
    'switch draft shopping carts' => array(
      'title' => t('Switch shopping cart from draft'),
      'description' => t('Allows users to switch shopping cart from drafts.'),
      'restrict access' => TRUE,
    ),
  );
  return $permissions;
}

/**
 * Implements hook_menu().
 */
function commerce_draft_carts_menu() {
  $items = array();

  $items['carts/switch'] = array(
    'title' => 'Switch Shopping Cart',
    'type' => MENU_CALLBACK,
    'page callback' => 'commerce_draft_carts_switch_cart',
    'page arguments' => array(2),
    'access arguments' => array('switch draft shopping carts'),
  );
  $items['carts/rename'] = array(
    'title' => 'Rename Shopping Cart',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_draft_carts_rename_form', 2),
    'access arguments' => array('switch draft shopping carts'),
  );
  $items['carts/delete'] = array(
    'title' => 'Delete Shopping Cart',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_draft_carts_delete_form', 2),
    'access arguments' => array('switch draft shopping carts'),
  );

  return $items;
}

/**
 * Function to switch the shopping cart from a draft.
 *
 * @param int $order_id
 *   The order id of the draft cart being fetched out.
 *
 * @return commerce_order
 *   The newly fetched shopping cart order
 */
function commerce_draft_carts_switch($order_id = 0) {
  global $user;
  if (!user_access('switch draft shopping carts')) {
    return FALSE;
  }

  $return_cart = NULL;
  // Save the current cart as draft and goto cart (for an empty cart).
  $current_cart = commerce_cart_order_load($user->uid);
  if (!($current_cart === FALSE)) {
    commerce_order_status_update($current_cart, 'cart_draft', FALSE);
  }

  $create_new = FALSE;
  if (!empty($order_id)) {
    $order = commerce_order_load($order_id);
    if (!($order === FALSE)) {
      // Change the order status of the draft to cart (normal).
      commerce_order_status_update($order, 'cart', FALSE);
      $create_new = FALSE;
      $cart_order_ids = &drupal_static('commerce_cart_order_load');
      $cart_order_ids[$user->uid] = $return_cart->order_id;
      commerce_cart_order_refresh($order);
      $return_cart = &$order;
    }
    else {
      $create_new = TRUE;
    }
  }
  else {
    $create_new = TRUE;
  }
  if ($create_new) {
    $new_order = commerce_draft_carts_draft_cart_new();
    if ($new_order === FALSE) {
      return FALSE;
    }
    commerce_order_status_update($new_order, 'cart', FALSE);
    $return_cart = &$new_order;
  }

  // Reset the static user => cart map.
  // drupal_static_reset('commerce_cart_order_load');
  $cart_order_ids = &drupal_static('commerce_cart_order_load');
  $cart_order_ids[$user->uid] = $return_cart->order_id;
  return $return_cart;
}

/**
 * Function to create a new draft shopping cart.
 *
 * @param string $cart_name
 *   The name of the draft cart being created.
 *
 * @return commerce_order
 *   The newly created draft shopping cart order
 */
function commerce_draft_carts_draft_cart_new($cart_name = '') {
  global $user;
  if (!user_access('switch draft shopping carts')) {
    return FALSE;
  }
  $order = commerce_order_new($user->uid, 'cart_draft');
  if ($order === FALSE) {
    return FALSE;
  }
  if (!empty($cart_name)) {
    // Change the name of the order.
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $wrapper->commerce_draft_cart_name->set($cart_name);
    commerce_order_save($order);
  }
  if (empty($_SESSION['draftcarts'])) {
    $_SESSION['draftcarts'] = array();
  }
  $_SESSION['draftcarts'][] = $order->order_id;
  return $order;
}

/**
 * Page call back function to switch the shopping cart to draft.
 *
 * @param int $order_id
 *   The order id of the draft cart being fetched out.
 */
function commerce_draft_carts_switch_cart($order_id = 0) {
  if (!user_access('switch draft shopping carts')) {
    return;
  }
  $cart = commerce_draft_carts_switch($order_id);
  if (($cart !== FALSE) && ($cart->order_id != $order_id)) {
    drupal_set_message(t('A new shopping cart is created'));
  }
  drupal_goto('cart');
}

/**
 * Shopping cart rename form.
 */
function commerce_draft_carts_rename_form($form, &$form_state, $order_id = 0) {
  if ($order_id == 0) {
    drupal_goto('cart');
    return;
  }
  $order = commerce_order_load($order_id);
  if ($order === FALSE) {
    drupal_goto('cart');
    return;
  }
  else {
    // Immediately unload to make the order available to everyone again.
    // Ref. https://drupal.org/node/1514618
    entity_get_controller('commerce_order')->resetCache(array($order->order_id));
  }

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  if (($wrapper->status->value() != 'cart') && ($wrapper->status->value() != 'cart_draft')) {
    drupal_set_message('Order is not a shopping cart. Rename action aborted.');
    drupal_goto('cart');
    return NULL;
  }
  $form = array();
  $cart_name = $wrapper->commerce_draft_cart_name->value();
  $form_state['#order'] = $order;
  $form['cartname'] = array(
    '#type' => 'textfield',
    '#default_value' => $cart_name,
    '#title' => t('Cart name'),
    '#size' => 20,
  );
  $form['rename'] = array(
    '#type' => 'submit',
    '#value' => t('Rename'),
  );
  $form['#submit'] = array('commerce_draft_carts_rename_form_submit');
  drupal_add_css(drupal_get_path('module', 'commerce_draft_carts') . '/css/form.css');
  return $form;
}

/**
 * Shopping cart rename form submit handler.
 */
function commerce_draft_carts_rename_form_submit($form, &$form_state) {
  $wrapper = entity_metadata_wrapper('commerce_order', $form_state['#order']);
  $status = $wrapper->status->value();
  if (($status == 'cart') || ($status == 'cart_draft')) {
    // Set the cart name.
    $original_name = $wrapper->commerce_draft_cart_name->value();
    if ($form_state['values']['cartname'] != $original_name) {
      $wrapper->commerce_draft_cart_name->set($form_state['values']['cartname']);
      commerce_order_save($form_state['#order']);
      drupal_set_message(t('Shopping cart is renamed to %name.', array('%name' => $form_state['values']['cartname'])));
    }
  }
  else {
    drupal_set_message('Order is not a shopping cart. Rename action is aborted.');
    drupal_goto('cart');
    return;
  }
  $form_state['redirect'] = 'cart';
}

/**
 * Shopping cart delete form.
 */
function commerce_draft_carts_delete_form($form, &$form_state, $order_id = 0) {
  if ($order_id == 0) {
    drupal_goto('cart');
    return;
  }
  $order = commerce_order_load($order_id);
  if ($order === FALSE) {
    drupal_goto('cart');
    return;
  }
  else {
    // Immediately unload to make the order available to everyone again.
    // Ref. https://drupal.org/node/1514618
    entity_get_controller('commerce_order')->resetCache(array($order->order_id));
  }

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  if (($wrapper->status->value() != 'cart') && ($wrapper->status->value() != 'cart_draft')) {
    drupal_set_message('Order is not a shopping cart. Delete action is cancelled.');
    drupal_goto('cart');
    return NULL;
  }
  $cartname = $wrapper->commerce_draft_cart_name->value();
  $line_items = $wrapper->commerce_line_items->value();
  if (count($line_items) > 0) {
    drupal_set_message('Shopping cart is not empty.', 'warning');
  }
  $form = array();
  $form['#submit'] = array('commerce_draft_carts_delete_form_submit');
  $form_state['#order'] = $order;
  drupal_add_css(drupal_get_path('module', 'commerce_draft_carts') . '/css/form.css');
  return confirm_form($form,
    t('Are you sure you want to delete the shopping cart %title?', array('%title' => $cartname)),
    'cart',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Shopping cart delete form submit handler.
 */
function commerce_draft_carts_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] == 'Delete') {
    $order = $form_state['#order'];
    $order_id = $order->order_id;
    // Delete the order.
    if (commerce_order_delete($order_id)) {
      // Remove the order_id from session.
      $session_cart_ids = array();
      if (!empty($_SESSION['draftcarts'])) {
        $session_cart_ids = $_SESSION['draftcarts'];
      }
      $_SESSION['draftcarts'] = array();
      foreach ($session_cart_ids as $session_cart_id) {
        if ($session_cart_id != $order_id) {
          $_SESSION['draftcarts'][] = $session_cart_id;
        }
      }

      $wrapper = entity_metadata_wrapper('commerce_order', $order);
      $cartname = $wrapper->commerce_draft_cart_name->value();
      drupal_get_messages();
      drupal_set_message(t('Shopping cart %name is deleted.', array('%name' => $cartname)));
      $cart_ids = commerce_draft_carts_get_cart_ids();
      $draft_order_id = 0;
      if (count($cart_ids > 0)) {
        $draft_order_id = array_shift($cart_ids);
      }
      commerce_draft_carts_switch_cart($draft_order_id);
      return;
    }
  }
  $form_state['redirect'] = 'cart';
}

/**
 * Implements hook_commerce_order_presave().
 *
 * Default the cart name to short datetime if it is empty.
 */
function commerce_draft_carts_commerce_order_presave($order) {
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $status = $wrapper->status->value();
  if (($status == 'cart') || ($status == 'cart_draft')) {
    $cart_name = $wrapper->commerce_draft_cart_name->value();
    if (empty($cart_name)) {
      // Set the cart name as date time in short format.
      $wrapper->commerce_draft_cart_name->set(format_date($wrapper->created->value(), 'short'));
    }
  }
}

/**
 * Cart selector block.
 */
function commerce_draft_carts_block_info() {
  $blocks = array();
  $blocks['commerce_draft_carts_switcher'] = array(
    'info' => t('Shopping Cart Draft Switcher'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function commerce_draft_carts_block_view($delta = '') {
  // The $delta parameter tells us which block is being requested.
  switch ($delta) {
    case 'commerce_draft_carts_switcher':
      $block['subject'] = '';
      $block['content'] = commerce_draft_carts_block_contents($delta);
      break;
  }
  return $block;
}

/**
 * Block contents.
 */
function commerce_draft_carts_block_contents($delta) {
  switch ($delta) {
    case 'commerce_draft_carts_switcher':
      return commerce_draft_carts_switcher();
  }
}

/**
 * Get the draft cart order ids of current user.
 *
 * @return array
 *   An array of order ids of draft carts.
 */
function commerce_draft_carts_get_cart_ids() {
  global $user;

  $result = array();
  if ($user->uid) {
    $result = db_query("SELECT order_id FROM {commerce_order} WHERE uid = :uid AND status = 'cart_draft' ORDER BY order_id DESC",
      array(':uid' => $user->uid))->fetchAll();
  }
  else {
    if ((isset($_SESSION['draftcarts'])) && (count($_SESSION['draftcarts']) > 0)) {
      $result = db_query("SELECT order_id FROM {commerce_order} WHERE order_id IN (:order_ids) AND uid = 0 AND hostname = :hostname AND status = 'cart_draft' ORDER BY order_id DESC",
        array(':order_ids' => $_SESSION['draftcarts'], ':hostname' => ip_address()))->fetchAll();
    }
  }
  $cart_ids = array();
  foreach ($result as $order) {
    $cart_ids[] = $order->order_id;
  }
  return $cart_ids;
}

/**
 * Output the content of the draft cart switcher.
 *
 * @return string
 *   The content of the cart switcher.
 */
function commerce_draft_carts_switcher() {
  global $user;
  if (!user_access('switch draft shopping carts')) {
    return;
  }

  $draftcarts = array();
  $output = '';

  $cart_ids = commerce_draft_carts_get_cart_ids();
  if (count($cart_ids) > 0) {
    $name_result = db_query("SELECT entity_id, commerce_draft_cart_name_value FROM {field_data_commerce_draft_cart_name} WHERE entity_type = 'commerce_order' AND entity_id IN (:order_ids) ORDER BY entity_id DESC", array(':order_ids' => $cart_ids))->fetchAll();

    foreach ($name_result as $order) {
      $draftcarts[] = array(
        'data' => l(check_plain($order->commerce_draft_cart_name_value), 'carts/switch/' . $order->entity_id),
        'id' => 'draft-cart-' . $order->entity_id,
        'class' => array('cart-switch-link'),
      );
    }
  }

  $title = '';

  $current_cart_order = commerce_cart_order_load($user->uid);
  $currentcart = '';
  if (!empty($current_cart_order->commerce_draft_cart_name[LANGUAGE_NONE][0]['safe_value'])) {
    $currentcart = l(check_plain($current_cart_order->commerce_draft_cart_name[LANGUAGE_NONE][0]['safe_value']), 'cart');
  }
  else {
    // If no name, use the date time as name for display.
    if (!empty($current_cart_order->created)) {
      $cart_datetime = format_date($current_cart_order->created, 'short');
      $currentcart = l($cart_datetime, 'cart');
    }
  }

  $renamelink = '';
  $deletelink = '';
  $create_title = t('Create a new cart');
  if (!empty($current_cart_order)) {
    $renamelink = l(t('rename'), 'carts/rename/' . $current_cart_order->order_id, array('attributes' => array('title' => t('Rename the shopping cart'))));
    $deletelink = l(t('delete'), 'carts/delete/' . $current_cart_order->order_id, array('attributes' => array('title' => t('Delete the shopping cart'))));
    $create_title = t('Save the cart as draft and create a new cart');
  }
  $createlink = l(('create'), 'carts/switch', array('attributes' => array('title' => $create_title)));
  $output = theme_commerce_draft_carts($title, $currentcart, $draftcarts, $renamelink, $deletelink, $createlink);
  return $output;
}

/**
 * Implements hook_theme().
 */
function commerce_draft_carts_theme() {
  $themes = array(
    'commerce_draft_carts' => array(
      // We use 'variables' when the item to be passed is an array whose
      // structure must be described here.
      'variables' => array(
        'title' => NULL,
        'currentcart' => NULL,
        'draftcarts' => NULL,
        'renamelink' => NULL,
        'deletelink' => NULL,
        'createlink' => NULL,
      ),
      'template' => 'commerce-draft-carts',
    ),
  );
  return $themes;
}

/**
 * Theming a draft shopping cart list.
 */
function theme_commerce_draft_carts($title, $currentcart, $draftcarts, $renamelink = '', $deletelink = '', $createlink = '') {
  $variables = array(
    'title' => $title,
    'currentcart' => $currentcart,
    'draftcarts' => $draftcarts,
    'renamelink' => $renamelink,
    'deletelink' => $deletelink,
    'createlink' => $createlink,
  );
  $output = theme('commerce_draft_carts', $variables);
  return $output;
}

/**
 * Template preprocess function for draft cart switcher.
 */
function commerce_draft_carts_preprocess_commerce_draft_carts(&$vars, $hook) {
  drupal_add_js(drupal_get_path('module', 'commerce_draft_carts') . '/js/draftcartswitcher.js', 'file');
  drupal_add_css(drupal_get_path('module', 'commerce_draft_carts') . '/css/draftcartswitcher.css');
}

/**
 * Implements hook_commerce_cart_order_id().
 */
function commerce_draft_carts_commerce_cart_order_id($uid) {
  $query = new EntityFieldQuery();
  $result = $query
    ->entityCondition('entity_type', 'commerce_order')
    ->propertyCondition('uid', $uid)
    ->propertyCondition('status', 'cart')
    ->execute();
  if (empty($result)) {
    // Switch to the last draft.
    $cart_ids = commerce_draft_carts_get_cart_ids();
    $draft_order_id = NULL;
    if (!empty($cart_ids)) {
      $draft_order_id = array_shift($cart_ids);
      $order = commerce_order_load($draft_order_id);
      if ($order !== FALSE) {
        commerce_order_status_update($order, 'cart', FALSE);
        return (int) $draft_order_id;
      }
    }
  };
  // Let the other modules to determine the cart id.
  return NULL;
}
