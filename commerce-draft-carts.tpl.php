<?php
/**
 * @file
 * Template file for commerce_draft_carts.
 */
?>
<!-- commerce_draft_carts template -->
<div class="commerce-draft-carts container-inline">
<?php if (!empty($title)): ?>
  <div class="title"><h3><?php print $title; ?></h3></div>
<?php endif; ?>
<?php if (!empty($currentcart)): ?>
  <div class="current-cart"><?php print $currentcart; ?></div>
<?php endif; ?>
<div class="carts-links">
<?php if (!empty($renamelink)): ?>
  <div class="link renamelink"><?php print $renamelink; ?></div>
<?php endif; ?>
<?php if (!empty($deletelink)): ?>
  <div class="link deletelink"><?php print $deletelink; ?></div>
<?php endif; ?>
<?php if (!empty($createlink)): ?>
  <div class="link createlink"><?php print $createlink; ?></div>
<?php endif; ?>
</div>
<?php if (!empty($draftcarts)): ?>
<div class="draft-carts">
<?php foreach($draftcarts as $id => $draftcart):  ?>
  <div class="draft-cart <?php print implode(' ', $draftcart['class']); ?>" id="<?php print $draftcart['id']; ?>"><?php print $draftcart['data']; ?></div>
<?php endforeach; ?>
</div>
<?php endif; ?>
</div>
<!-- /commerce_draft_carts template -->
