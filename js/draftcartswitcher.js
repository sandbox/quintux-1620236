(function($) {

  Drupal.CommerceDraftCarts = Drupal.CommerceDraftCarts || {};

  Drupal.behaviors.commerceDraftCarts = {

    attach: function (context, settings) {
      function showDraftCartLinks(div) {
        draftcarts = $(div).parentsUntil('.commerce-draft-carts').parent().children('.draft-carts');
        currentcart = $(div).parentsUntil('.commerce-draft-carts').parent().children('.current-cart');
        position = currentcart.position();
        height = currentcart.height();
        draftcarts.removeClass('fadingout');
        draftcarts.css('position', 'absolute');
        draftcarts.css('top', position.top + height);
        draftcarts.css('left', position.left);
        draftcarts.css('z-index', '999');
        draftcarts.fadeIn(200);
      }
      function hideDraftCartLinks(div) {
        draftcarts = $(div).parentsUntil('.commerce-draft-carts').parent().children('.draft-carts');
        if ((! draftcarts.is(':hidden')) && (! draftcarts.hasClass('fadingout'))) {
          draftcarts.addClass('fadingout');
          setTimeout(function() {
            draftcarts = $('.commerce-draft-carts .draft-carts.fadingout');
            draftcarts.fadeOut(100);
            draftcarts.removeClass('fadingout');
          },
          2000);
        }
      }
      $('.commerce-draft-carts').children('.draft-carts').hide();
      $('.commerce-draft-carts .current-cart a').bind('mouseover', function () {
        showDraftCartLinks(this);
      });
      $('.commerce-draft-carts .current-cart a').bind('mouseout', function () {
        hideDraftCartLinks(this);
      });
      $('.commerce-draft-carts .draft-carts a').bind('mouseover', function () {
        showDraftCartLinks(this);
      });
      $('.commerce-draft-carts .draft-carts a').bind('mouseout', function () {
        hideDraftCartLinks(this);
      });
    }
  };
})(jQuery);
