Commerce Draft Carts

Allows users to create and manage draft shopping carts in Drupal Commerce 
enabled sites.

How to use:
 - Enable the module
 - Put the 'Shopping Cart Draft Switcher' block to the cart page 
   or any page you want.

A new order status is added: Shopping cart: Draft (cart_draft)
A new field is added to orders: Cart name (commerce_draft_cart_name)


TODO:
 - A better UI to manage draft carts
 - Draft cart cleanup logics
 - Use token for default cart names
 - Rules integration
